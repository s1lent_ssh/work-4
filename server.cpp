#include <boost/asio.hpp>
#include <iostream>
#include <vector>
#include <thread>
#include <future>
#include <queue>

using namespace boost;
using namespace asio::ip;
using namespace std;

const auto SLEEP_TIME = 2s;
const int MAX_THREADS = 10;
const int PORT = 8001;
const int BUFFER_SIZE = 5;
const string LOG_NAME = "[Server] ";

std::mutex _mutex;
std::vector<std::thread> _threads;
std::queue<tcp::socket> _queue;

int main() {
	asio::io_service _service;
	tcp::endpoint _endpoint(tcp::v4(), PORT);
	tcp::acceptor _acceptor(_service, _endpoint);

	auto process = [](auto&& socket, const auto& lambda) {
		_threads.emplace_back(
			std::thread([](auto&& socket, const auto& lambda) {
				//Handle connection
				char buffer[BUFFER_SIZE];
				cout << LOG_NAME << "(Accepted) Threads count: " << _threads.size() << endl;
				std::this_thread::sleep_for(SLEEP_TIME);
				auto bytes = socket.read_some(asio::buffer(buffer));
				std::string message(buffer, bytes);
				cout << LOG_NAME << "Got message: \"" << message << "\"" << endl;
				socket.write_some(asio::buffer("Echo " + message));

				//Remove thread
				std::async([](const auto id, const auto& lambda){
					std::lock_guard lock(_mutex);
					auto iter = std::find_if(_threads.begin(), _threads.end(), [=](const auto& t) { return t.get_id() == id; });
					if(iter != _threads.end()) {
						iter->detach();
						_threads.erase(iter);
					}
					if(!_queue.empty()) {
						lambda(std::move(_queue.front()), lambda);
						_queue.pop();
					}
				}, std::this_thread::get_id(), lambda);

			}, std::move(socket), lambda)
		);
	};

	while(true) {
		tcp::socket _socket(_service);
		_acceptor.accept(_socket);

		if(_threads.size() == MAX_THREADS) {
			_queue.push(std::move(_socket));
		} else {
			process(std::move(_socket), process);
		}
	}
}

